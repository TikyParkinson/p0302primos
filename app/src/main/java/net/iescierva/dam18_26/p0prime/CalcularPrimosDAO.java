package net.iescierva.dam18_26.p0prime;

import java.util.ArrayList;

public class CalcularPrimosDAO {

    private static CalcularPrimosDAO instance;
    private static ArrayList<Integer> listaPrimos;

    /**
     * Método estático de acceso a la instancia única. Si no existe la crea
     * invocando al constructor interno. Utiliza inicialización diferida. Sólo se
     * crea una vez; instancia única -patrón singleton-
     *
     * @return instance
     */
    public static CalcularPrimosDAO getInstance() {
        if (instance == null) {
            instance = new CalcularPrimosDAO();
        }
        return instance;
    }

    /**
     * Constructor por defecto de uso interno. Sólo se ejecutará una vez.
     */
    private CalcularPrimosDAO() {
        listaPrimos = new ArrayList<>();
    }

    public void buscarPrimo(int primo) {
       obtenerNumerosPrimos(primo);
    }

    public int get(int primo) {
        return listaPrimos.get(primo - 1);
    }

    public int ultimoPrimo() {
        return listaPrimos.get(listaPrimos.size()-1);
    }

    private void obtenerNumerosPrimos(int primo){
        int ultimoNumeroPrimo = 0;
        if (primo <= listaPrimos.size())
            return;
        if (listaPrimos.size() == 0)
            listaPrimos.add(2);
        if (listaPrimos.size() == 1)
            ultimoNumeroPrimo = 3;
        else
            ultimoNumeroPrimo = ultimoPrimo()+2;

        while (primo > listaPrimos.size()) {
            int contador = 0;
            boolean salir = true;
            boolean isPrime = true;
            do {
                while ( contador <= listaPrimos.size() - 1  && salir && isPrime) {
                        salir = listaPrimos.get(contador).intValue() <= Math.sqrt(ultimoNumeroPrimo);
                        if (salir) {
                            if (ultimoNumeroPrimo % listaPrimos.get(contador) == 0)
                                 isPrime = false;
                            contador++;
                        }
                }
            } while (isPrime && primo > listaPrimos.size() && salir);

            if (isPrime)
                listaPrimos.add(ultimoNumeroPrimo);
            ultimoNumeroPrimo += 2;

        }
    }
}
