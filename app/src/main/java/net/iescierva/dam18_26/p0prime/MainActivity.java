package net.iescierva.dam18_26.p0prime;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button botonPrimo;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonPrimo = findViewById(R.id.botonPrimo);
        botonPrimo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        EditText editNumberPrimo = findViewById(R.id.editTextNumberPrimos);
        if (editNumberPrimo.getText().toString().equals(""))
            editNumberPrimo.setError("Error, introduce al menos un número.");
        else if (editNumberPrimo.getText().toString().equals("0"))
            editNumberPrimo.setError("Error no se puede introducir el numero cero");
        else
            añadirPrimoEditText();
    }

    private void añadirPrimoEditText(){
        CalcularPrimosDAO calculaPri;
        EditText editNumberPrimo = findViewById(R.id.editTextNumberPrimos);
        EditText editResultado = findViewById(R.id.editTextResultadoPrimo);
        String numeroObtenido;
        int numeroIntroducido = Integer.valueOf(editNumberPrimo.getText().toString());

        calculaPri = CalcularPrimosDAO.getInstance();
        calculaPri.buscarPrimo(numeroIntroducido);
        numeroObtenido = String.valueOf(calculaPri.get(numeroIntroducido));
        editResultado.setText(numeroObtenido);
    }

}
